#!/usr/bin/env bash

# This script will run all tests and store the results in a file

if ! command -v /usr/bin/time &> /dev/null
   then
       echo "This script requires the GNU time tool at /usr/bin/time (https://www.gnu.org/software/time/)"
       exit 1
fi

if ! command -v inxi &> /dev/null
   then
       echo "inxi not installed. Will not log system information."
       system="NA"
       cp_host="NA"
   else
       system=$(inxi -S | xargs | awk -F 'Desktop:' '{ print $2 }')
       cp_host=$(inxi -c 0 | xargs)
fi

timestamp=$(date "+%Y%m%d-%H%M%S")
out_file="results.csv"

function pre_test() {
    if ! command -v $1 &> /dev/null
    then
        echo "${1} not installed, skipping ${2} test..."
        echo "=====//====="
        installed=0
    else
        installed=1
    fi
}

function run_test() {
    pre_test $1
    if [ $installed -eq 1 ]
    then
        language=${*: -1}
        if [ "$language" = "C" ]
           then
               version=$(gcc --version | sed '1,1!d')
        else
            version=$( { $1 --version ; } 2>&1 | sed '1,1!d')
        fi
        echo "Testing... ${language} = ${version}"
        result=$( { /usr/bin/time -f %E ${@:1:$#-1} ; } 2>&1 | sed '3,3!d')
        echo "Elapsed time (mm:ss:ms) ${result}"
        echo "=====//====="
        echo "${timestamp},${cp_host},Desktop:${system},${language},${version},${result}" >> $out_file
    fi
}

run_test sbcl --script counter.lisp "Common Lisp"
run_test java counter.java "Java"
run_test python3 counter.py "Python"
run_test Rscript --vanilla counter.R "R"
run_test julia counter.jl "Julia"

gcc -o counter counter.c
run_test ./counter "C"
