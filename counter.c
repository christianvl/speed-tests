#include <stdio.h>
int main() {
  int n = 0;
  while(n < 1000000000) {
    if (n == 0) {
      printf("First...\n");
    }
    if (n == 999999999) {
      printf("...Last\n");
    }
    n++;
  }
  return 0;
}
